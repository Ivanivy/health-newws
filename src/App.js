import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './styles/global.css';
import { Route, BrowserRouter, Switch, withRouter } from 'react-router-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
//importing components
import Login from './pages/Login';
import Register from './pages/Register';
import Panel from './pages/Panel';

function getPathDepth(location) {
  return (location || {}).pathname.split('/').length;
}


class App extends Component {

  state = {
    prevDepth: null
  }

  componentDidMount() {
    this.setState({ prevDepth: getPathDepth(this.props.location) });
  }



  render() {
    return (
      <BrowserRouter>
        <Route render={({ location }) => (
          <TransitionGroup>
            <CSSTransition timeout={400}
              classNames={'pageSliderRight'}
              mountOnEnter={true}
              unmountOnExit={true}
              key={location.key}
            >
              <Switch location={location}>
                <Route path="/" exact>
                  <Login />
                </Route>
                <Route path="/register" exact>
                  <Register />
                </Route>
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        )} />
        <Route path="/home" >
          <Panel />
        </Route>
      </BrowserRouter>
    );
  }
}

export default withRouter(App);
