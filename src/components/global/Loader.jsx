import React from 'react'
import styles from '../../styles/loader.module.css';

export default function Loader({ message }) {
    return (
        <div className={`${styles.loaderContainer}`}>
            <div className={`${styles.box}`}>
                <span></span>
                <span></span>
            </div>
            <div>
                {message} ...
                </div>
        </div>
    )
}
