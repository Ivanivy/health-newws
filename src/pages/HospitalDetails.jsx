import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Menu, Card, List, Icon, MenuItem, Modal, ModalActions, Button, Form, Radio } from 'semantic-ui-react'
import styles from '../styles/hospitalDetails.module.css'
import { Route, Link, withRouter } from 'react-router-dom'
import Loader from '../components/global/Loader';
import { baseUrl, endpoints } from '../utils/config'
import axios from 'axios';
import { result } from 'lodash'
import { Animated } from 'react-animated-css';
class HospitalDetails extends Component {

    state = {
        selectedTab: 1,
        open: false,
        message: "",
        data: null,
        selected: null,
        selectedPersonnel: null,
        open: false,
        open2: false,
        dates: {

        }
    }

    menuItemHandlClick = (tabId, path) => {
        this.props.history.push(path);
        this.setState({ selected: tabId });
    }

    handlePlanRendezVous = () => {
        this.setState({ open2: true })
        axios.post(baseUrl + endpoints.createRendezVosu, {
            nompu: JSON.parse(localStorage.getItem("data")).username,
            nomPres: this.state.value,
            idu: JSON.parse(localStorage.getItem("data")).id,
            idprestation: this.state.idPres,
            idspecialite: this.state.selected.id
        }).then(response => {
            console.log(response);
            this.setState({ open2: false });
            this.props.history.push("/home/rendezvous");
        }).catch(error => {
            console.log(error);
        })
    }

    fetchData = () => {
        console.log(`${baseUrl}${endpoints.speicialites}1`)
        var config = {
            method: 'get',
            url: `${baseUrl}${endpoints.speicialites}${this.props.match.params.id}`,
            headers: {}
        };
        this.setState({ open: true })
        axios(config)
            .then((response) => {
                console.log(response.data);
                this.setState({ data: response.data })
                this.setState({ open: false })
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    componentDidMount() {

        this.fetchData();

    }

    setSelected = (selected) => {
        console.log(selected);
        this.setState(
            {
                selected
            }
        )
    }
    setSelectedPerson = (selectedPersonnel) => {
        console.log(selectedPersonnel);
        this.setState({
            selectedPersonnel
        });
    }

    handleChange = (e, { value }) => {
        this.setState({ value });
        if (value == 'consultation') {
            this.setState({ idPres: 1 });
        } else {
            this.setState({ idPres: 2 });
        }
    }

    render() {
        console.log("visualisation state");
        console.log(this.state.data);
        console.log(this.props)
        let mappedSpecialities;
        let mappedPersonnel;
        if (this.state.data) {
            mappedSpecialities = this.state.data.listSpecialites.map(element =>

                <div className={`${styles.cardSize} card margin-top margin-bottom`}>
                    <div><Icon name="sticky note outline" /></div>
                    <div>
                        <div>
                            <Link onClick={() => { this.setSelected(element); this.props.history.push(`${this.props.match.params.name}/personnel`) }}>
                                {element.nom}
                            </Link>
                        </div>
                        <div>
                            {element.description}
                        </div>
                    </div>
                    <div>
                        <Icon name="plus circle" />
                    </div>
                </div>
            );
        }

        if (this.state.selected) {
            mappedPersonnel = this.state.selected.listPersonnels.map(element =>
                <div className={`${styles.cardSize} card margin-top margin-bottom`}>
                    <div><Icon name="doctor" /></div>
                    <div>
                        <div>
                            <Link onClick={() => { this.setSelectedPerson(element); this.setState({ open: true }) }}>
                                {element.nom}
                            </Link>
                        </div>
                        <div>
                            <div><strong>Grade:</strong>{element.grade}</div>
                            <div><strong>Telephone:</strong>{element.telephone}</div>
                            <div><strong>Mail:</strong>{element.mail}</div>
                        </div>
                    </div>

                </div>);
        }

        return (
            <div className={`${styles.pageContainer}`}>
                <h2>{this.props.match.params.name}</h2>
                <Menu>
                    <MenuItem onClick={() => { this.menuItemHandlClick(1, "/home/hospital/specialite/:id/:name") }} active={this.state.selectedTab == 1 ? true : false}>
                        Spécialité
                    </MenuItem>
                </Menu>

                <Route path="/home/hospital/specialite/:id/:name" exact>

                    {mappedSpecialities}
                    <Modal closeOnDimmerClick={false} open={this.state.open}>
                        <Loader message={"Chargement des spécialités"} />
                    </Modal>
                    <Modal closeOnDimmerClick={false} open={this.state.open2}>
                        <Loader message={"Planification"} />
                    </Modal>
                </Route>
                <Route path="/home/hospital/specialite/:id/:name/personnel" exact>
                    <Animated  animationIn="fadeIn" animationOut="fadeOut" isVisible={true}>
                        <Modal open={this.state.open}>
                            <div style={{ margin: "1rem", fontSize: "1.15rem" }}>
                                Voulez vous planifier un rendez vous avec Dr <strong>{this.state.selectedPersonnel ? this.state.selectedPersonnel.nom : null}</strong> , spécialiste en {this.state.selectedPersonnel ? this.state.selectedPersonnel.specialite : null}?
                        </div>
                            <ModalActions>
                                <Button onClick={() => { this.props.history.push(`prestation`) }} >Oui</Button>
                                <Button onClick={() => { this.setState({ open: false }) }} negative>Non</Button>
                            </ModalActions>
                        </Modal>
                    </Animated>
                    {mappedPersonnel}
                </Route>
                <Route path="/home/hospital/specialite/:id/:name/prestation" exact>
                    <div style={{ marginTop: "1.5rem" }}>
                        <Form>
                            <Form.Field>
                                <strong>Chosir la prestation:</strong>
                            </Form.Field>
                            <Form.Field>
                                <Radio
                                    label='Consultation (3000 FCFA)'
                                    name='radioGroup'
                                    value='consultation'
                                    checked={this.state.value === 'consultation'}
                                    onChange={this.handleChange}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Radio
                                    label='Operation (50 000 FCFA)'
                                    name='radioGroup'
                                    value='operation'
                                    checked={this.state.value === 'operation'}
                                    onChange={this.handleChange}
                                />
                            </Form.Field>
                            <div style={{ marginTop: "1.5rem" }}>
                                <Button onClick={this.handlePlanRendezVous}>Valider</Button>
                            </div>
                        </Form>
                    </div>
                </Route>
            </div >
        )
    }
}
export default withRouter(HospitalDetails);