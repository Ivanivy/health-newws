import React, { Component } from 'react';
import styles from '../styles/login.module.css';
import { Input, Button, Modal, Icon } from 'semantic-ui-react';
import { Link, withRouter } from 'react-router-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import axios from 'axios';
import { baseUrl, endpoints } from '../utils/config';


/* importing assets */
import { ReactComponent as Logo } from '../assets/images/logo.svg';
import Loader from '../components/global/Loader';
const FIELD_NAMES = {
    username: "username",
    password: "passowrd",
};
class Login extends Component {


    state = {
        username: "",
        password: "",
        open: false,
        open2: false
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = () => {
        this.setState({ open: true });
        axios.post(`${baseUrl}${endpoints.login}`,
            {
                username: this.state[FIELD_NAMES.username],
                password: this.state[FIELD_NAMES.password],
            }
        ).then(response => {
            console.log(response.data)
            setTimeout(() => {
                localStorage.setItem("data", JSON.stringify(response.data));
                localStorage.setItem("token", response.data.token)

                this.props.history.push("/home");
            }, 2500);
        }).catch(error => {
            console.log(error)
            this.setState({ open2: true })
        });
    }

    render() {
        return (
            <div className={[styles.loginContainer]}>
                <div className={[styles.logoContainer]}>
                    <div>
                        <Logo />
                    </div>
                    <div>
                        Health News
                    </div>
                </div>
                <div className={[styles.formContainer]}>
                    <div className={`${styles.loginTitle} text1-semi-bold`}>
                        Login
                    </div>
                    <div className={`${styles.formExplanation} text3`} >
                        Entrez vos informations de connexion pour continuer
                    </div>
                    <div className={`${styles.inputContainer} full-width`}>
                        <div className={[], ""}>
                            <Input placeholder="Nom d'utilisateur" name={FIELD_NAMES.username} onChange={this.handleChange} />
                        </div>
                        <div>
                            <Input placeholder="Mot de passe" name={FIELD_NAMES.password} onChange={this.handleChange} type="password" />
                        </div>
                    </div>
                    <div className={`${styles.buttonContainer}`}>
                        <Modal trigger={<Button className={`margin-bottom`} onClick={this.handleSubmit}>
                            Suivant
                        </Button>} closeOnDimmerClick={false} open={this.state.open}>
                            <Loader message={"Connexion"} />
                            <Modal open={this.state.open2}>
                                <h4 style={{ margin: "2rem" }}>Nom d'utilisateur ou mot de passe incorrect <Icon name="close" color="red" /></h4>
                                <Modal.Actions>
                                    <Button onClick={() => { this.setState({ open2: false }); this.setState({ open: false }) }} negative>Réessayer</Button>
                                    <Button onClick={() => { this.props.history.push("/register") }}>S'inscire</Button>
                                </Modal.Actions>
                            </Modal>
                        </Modal>

                        <div style={{ marginTop: "1.3rem" }}>
                            <Link to="/register">
                                S'enregistrer
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(Login);