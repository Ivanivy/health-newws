import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/map.module.css';

export default class Map extends Component {
    static propTypes = {
        prop: PropTypes
    }

    render() {
        return (
            <div className={`${styles.pageContainer}`}>
                <div class="pt-3">
                    <div className={`${styles.canvasForGoogleMap}`} ><iframe  frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=3.818976,+11.488494&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe>
                    </div>
                </div>
            </div>
        )
    }
}
