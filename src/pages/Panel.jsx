import React, { Component } from 'react'
import styles from '../styles/panel.module.css';
import { Icon, Loader, Search } from 'semantic-ui-react';

/* importing comopnents */
import SearchPage from './SearchPage'
import HospitalDetails from './HospitalDetails';
import Publication from './Publication';
import PublicationDetails from './PublicationDetails';
import PublicationAdd from './PublicationAdd';
import { withRouter, Link, Route, Switch } from 'react-router-dom';
import User from './User';
import RendezVous from './RendezVous';
import Map from './Map';

const Panel = ({ history }) => {
    return (
        <div className={`${styles.panelContainer} full-height-screen`}>
            <div className={`${styles.titleBar}`}>
                <Link onClick={() => { history.goBack(); }}>
                    <Icon name="arrow left" size={5} />
                </Link>
                <Icon name="doctor" />
                <div>
                    Health News
                </div>
            </div>
            <div>

            </div>
            <Switch>
                <Route path="/home" exact>
                    <SearchPage styles={styles} />
                </Route>
                <Route path="/home/loading" exact>
                    <Loader />
                </Route>
                <Route path="/home/publication" exact>
                    <Publication />
                </Route>
                <Route path="/home/map" exact>
                    <Map />
                </Route>
                <Route path="/home/publication/details" exact>
                    <PublicationDetails />
                </Route>
                <Route path="/home/publication/add" exact>
                    <PublicationAdd />
                </Route>
                <Route path="/home/user" >
                    <User />
                </Route>
                <Route path="/home/rendezvous" exact>
                    <RendezVous />
                </Route>
                <Route path="/home/hospital/specialite/:id/:name" exact>
                    <HospitalDetails />
                </Route>
                <Route path="/home/hospital/specialite/:id/:name/personnel" exact>
                    <HospitalDetails />
                </Route>
                <Route path="/home/hospital/specialite/:id/:name/prestation" exact>
                    <HospitalDetails />
                </Route>
                <Route path="/home/hospital/planifier" exact>
                    <HospitalDetails />
                </Route>
            </Switch>

            <div className={`${styles.bottomMenuContainer}`}>
                <nav className={`${styles.nav}`}>
                    <Link to="/home" className={`${styles.navLink} `}>
                        <Icon name="search" />
                    </Link>
                    <Link to="/home/publication" className={`${styles.navLink} `}>
                        <Icon name="plus" />
                    </Link>
                    <Link to="/home/map" className={`${styles.navLink} `}>
                        <Icon name="map outline" />
                    </Link>
                    <Link to="/home/rendezvous" className={`${styles.navLink} `}>
                        <Icon name="calendar outline" />
                    </Link>
                    <Link to="/home/user/transactions" className={`${styles.navLink} `}>
                        <Icon name="user outline" />
                    </Link>
                </nav>
            </div>

        </div >
    )
}

export default withRouter(Panel);