import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Plan extends Component {
    static propTypes = {
        prop: PropTypes
    }

    render() {
        return (
            <div>
                <List divided relaxed>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header>
                                <Link to="/home/hospital">
                                    Hôpital Haluchiem
                                </Link>
                            </List.Header>
                            <List.Description >Yaoundé-Obobogo</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Etoug ebe</List.Header>
                            <List.Description >Yaoundé -Etoug Ebe</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Centre de sante la grâce</List.Header>
                            <List.Description >Yaoundé- Melen</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Hôpital Haluchiem</List.Header>
                            <List.Description >Yaoundé-Obobogo</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Etoug ebe</List.Header>
                            <List.Description >Yaoundé -Etoug Ebe</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Centre de sante la grâce</List.Header>
                            <List.Description >Yaoundé- Melen</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Hôpital Haluchiem</List.Header>
                            <List.Description >Yaoundé-Obobogo</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Etoug ebe</List.Header>
                            <List.Description >Yaoundé -Etoug Ebe</List.Description>
                        </List.Content>
                    </List.Item>
                    <List.Item>
                        <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Header >Centre de sante la grâce</List.Header>
                            <List.Description >Yaoundé- Melen</List.Description>
                        </List.Content>
                    </List.Item>
                </List>
            </div>
        )
    }
}
