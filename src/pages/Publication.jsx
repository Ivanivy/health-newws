import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/publication.module.css';
import { Icon, Image, Modal } from 'semantic-ui-react';
import Loader from '../components/global/Loader';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { baseUrl, endpoints } from '../utils/config';
import {Animated} from 'react-animated-css'
export default class Publication extends Component {

    state = {
        open: false
    }

    fetchPublication = () => {
        axios.get(`${baseUrl}${endpoints.publications}1`).then(result => {
            this.setState({ publications: result })
            this.setState({ open: false })
        }).catch(error => {
            console.log(error);
        })
    }
    componentDidMount() {
        this.setState({ open: true })
        this.fetchPublication();

    }
    render() {
        let mappedPublications;
        if (this.state.publications) {
            mappedPublications = this.state.publications.data.map(element => {
                console.log(element)
                return (<div className={`${styles.newsFeed}`}>
                    <div>
                        <div className={``}>
                            <h4>
                                <Link to={{
                                    pathname: "/home/publication/details/",
                                    data: element
                                }}>
                                    {element.titre}
                                </Link>
                            </h4>
                        </div>
                        <div>{element.date}</div>
                    </div>
                    <div style={{ textAlign: "" }}>
                        {element.contenue}
                    </div>
                    <div>
                        <Image src={`data:image/jpeg;base64,${element.imagemodel.picByte}`} />
                    </div>
                </div>)
            }
            );
        }

        return (
            <div className={`${styles.pageContainer}`}>
                <div>
                    <div>
                        Publications
                    </div>
                    {/* <div>
                        <Link to="/home/publication/add">
                            <Icon name="plus circle" />
                        </Link>
                    </div> */}
                </div>
                <Animated animationIn="fadeInUp" animationOut="fadeOut" isVisible={true}>
                    <div className={`${styles.newsFeedContainer}`}>
                        {mappedPublications}
                        <Modal closeOnDimmerClick={false} open={this.state.open}>
                            <Loader message={"Chargement des publications"} />
                        </Modal>
                    </div>
                </Animated>
            </div>
        )
    }
}
