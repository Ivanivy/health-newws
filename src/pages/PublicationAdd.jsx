import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/publicationAdd.module.css'
import { Modal, Image, Icon, Input, Button } from 'semantic-ui-react'
import Loader from '../components/global/Loader';
import { baseUrl, endpoints } from '../utils/config'
import axios from 'axios'
export default class PublicationAdd extends Component {

    state = {
        file: 'https://react.semantic-ui.com/images/wireframe/image.png',
        open: false,
        open2: false
    }

    handleChange = (event) => {
        console.log(event.target);
        this.setState({
            file: URL.createObjectURL(event.target.files[0])
        })
    }

    handleChangeText = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = () => {
        this.setState({ open: true })
        var formData = new FormData();
        formData.append("publication", this.state.file);
        var config = {
            method: 'post',
            url: `${baseUrl}${endpoints.publish}idStruc=${1}&titre=${this.state.title}&contenue=${this.state.description}`,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("token"),
                // ...formData.getHeaders()
            },
            data: formData
        };
        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                this.setState({ open2: true });
                setTimeout(() => {

                    this.setState({ open: false })
                    this.setState({ open2: false })
                }, 1500)
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    render() {
        return (
            <div className={`${styles.pageContainer}`}>
                <div>
                    <div>
                        Ajout Publication
                    </div>
                </div>
                <div className={`${styles.formContainer}`}>
                    <div>
                        <div>
                            <Input placeholder="Titre" name="title" onChange={this.handleChangeText} />
                        </div>
                        <div>
                            <Input placeholder="Description" name="description" onChange={this.handleChangeText} />
                        </div>
                    </div>
                    <div className={`${styles.uploadContainer}`}>
                        <div className={`${styles.uploadBtnWrapper}`}>
                            <button className={`${styles.btn}`}>Choisir une image</button>
                            <input type="file" name="myfile" onChange={this.handleChange} />
                        </div>
                        <div className={`${styles.preview}`}>
                            <img src={this.state.file} alt="" srcset="" />
                        </div>
                    </div>
                    <div>
                        <Modal open={this.state.open} trigger={<Button onClick={this.handleSubmit}>
                            Publier
                        </Button>}>
                            <Loader message={"Publication"} />
                            <Modal open={this.state.open2}>
                                <h4 style={{ margin: "2rem" }}>Publié avec succès <Icon name="check" color="green" /></h4>
                            </Modal>
                        </Modal>

                    </div>
                </div>
            </div >
        )
    }
}
