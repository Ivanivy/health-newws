import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom';
import styles from '../styles/publicationDetails.module.css'
import { Button, Form, Icon, Image, Input } from 'semantic-ui-react'
import { baseUrl, endpoints } from '../utils/config';
import axios from 'axios';

class PublicationDetails extends Component {

    state = {
        pubId: this.props.location.data,
        newComment: []
    }
    commentPublication = (comment, pubid) => {
        var data = JSON.stringify({
            utilisateur: JSON.parse(localStorage.getItem("data")).username,
            contenu: comment
        });
        this.setState({ newComment: [...this.state.newComment, { comment }] });
        var config = {
            method: "post",
            url: baseUrl + endpoints.comment + pubid,
            headers: {
                'Content-Type': 'application/json',
            },
            data: data
        }

        axios(config).then(response => {
            console.log(response);
            // this.props.history.push("/home/publication");
        }).catch(error => {
            console.log(error);
        })
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    render() {
        const { data } = this.props.location;
        console.log(this.props.location.data);
        let mappedComments = data.listCommentaires.map(element => {
            return (
                <div className={`${styles.commentItem}`}>
                    <div>
                        <Icon name="user circle " />
                    </div>
                    <div className={`${styles.comment}`}>
                        <strong>{element.utilisateur}</strong><br />
                        {element.contenu}
                    </div>
                </div>
            )
        });

        let mappedNewComments = this.state.newComment.map(element => {
            return (
                <div className={`${styles.commentItem}`}>
                    <div>
                        <Icon name="user circle " />
                    </div>
                    <div className={`${styles.comment}`}>
                        <strong>{JSON.parse(localStorage.getItem("data")).username}</strong><br />
                        {element.comment}
                    </div>
                </div>
            )
        })
        return (
            <div className={`${styles.pageContainer}`}>
                <div className={`${styles.newsFeed}`}>
                    <div>
                        <div className={``}>
                            <h3>
                                {data.titre}
                            </h3>
                        </div>
                        <div>{data.date}</div>
                    </div>
                    <div>
                        {data.contenue}
                    </div>
                    <div>
                        <Image src={`data:image/jpeg;base64,${data.imagemodel.picByte}`} />
                    </div>
                </div>
                <div style={{ width: "91%" }}>
                    <div className={`${styles.comments}`}>
                        {mappedComments}
                        {mappedNewComments}
                    </div>
                    <div className="answer">
                        <div className={`${styles.commentForm}`}>
                            <Input name="commentField" placeholder={`commenter`} onChange={this.handleChange} />
                            <Link to={{
                                data: data
                            }} onClick={() => { this.commentPublication(this.state.commentField, data.id) }}>
                                <Icon name="arrow circle right" />
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default withRouter(PublicationDetails);