import React, { Component } from 'react';
import styles from '../styles/register.module.css';
import { withRouter } from 'react-router-dom';
import { Icon, Input, Button, Modal } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { baseUrl, endpoints } from '../utils/config';
import axios from 'axios';


/* importing assets */
import { ReactComponent as Logo } from '../assets/images/logo.svg';
import Loader from '../components/global/Loader';

const FIELD_NAMES = {
    username: "username",
    email: "email",
    password: "passowrd",
    vpassword: "verfiedPassword"
};

class Register extends Component {
    state = {
        [FIELD_NAMES.username]: "",
        [FIELD_NAMES.email]: "",
        [FIELD_NAMES.password]: "",
        "role": ["au"],
        open: false,
        open2: false,
    }
    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmit = () => {
        this.setState({ open: true });
        axios.post(`${baseUrl}${endpoints.register}`,
            {
                username: this.state[FIELD_NAMES.username],
                email: this.state[FIELD_NAMES.email],
                password: this.state[FIELD_NAMES.password],
                role: ["au"]
            }
        ).then(response => {
            console.log(`response from server: ${response.data}`)
            this.setState({ open2: true });
            setTimeout(() => {
                this.props.history.push("/");
            }, 2500);
        }).catch(error => {
            console.log(error)
            this.setState({ open: false })
        });
    }
    getFieldValue(fieldName) {
        return this.state[fieldName]
    }

    render() {
        return (
            <div className={[styles.loginContainer]}>
                <div className={[styles.logoContainer]}>
                    <div>
                        Health News
                    </div>
                </div>
                <div className={[styles.formContainer]}>
                    <div className={`${styles.loginTitle} text1-semi-bold`}>
                        Inscription
                    </div>
                    <div className={`${styles.formExplanation} text3`} >
                        Entrez vos informations de connexion pour continuer
                    </div>
                    <div className={`${styles.inputContainer} full-width`}>
                        <div className={[], ""}>
                            <Input placeholder="Nom d'utilisateur" name={FIELD_NAMES.username} onChange={this.handleChange} />
                        </div>
                        <div className={[], ""}>
                            <Input placeholder="Email" name={FIELD_NAMES.email} onChange={this.handleChange} />
                        </div>
                        <div>
                            <Input type="password" placeholder="Mot de passe" name={FIELD_NAMES.password} onChange={this.handleChange} />
                        </div>
                        <div>
                            <Input type="password" placeholder="Confirmer mot de passe" name={FIELD_NAMES.vpassword} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className={`${styles.buttonContainer}`}>
                        <Modal trigger={<Button className={`margin-bottom`} onClick={this.handleSubmit}>S'enregistrer</Button>} closeOnDimmerClick={false} open={this.state.open}>
                            <Loader message={"Inscription"} />
                            <Modal open={this.state.open2}>
                                <h4 style={{ margin: "3rem" }}>Enregistré avec succès <Icon name="check" color="green" /></h4>
                            </Modal>
                        </Modal>

                        <div style={{ marginTop: "1.3rem" }}>
                            <Link to="/">
                                Se connecter
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(Register);