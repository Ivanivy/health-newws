import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/rendezVous.module.css';
import { Icon, Modal } from 'semantic-ui-react';
import { Route } from 'react-router-dom';
import axios from 'axios';
import Loader from '../components/global/Loader';
import { baseUrl, endpoints } from '../utils/config';
import { Animated } from 'react-animated-css';
/* importing components */

export default class RendezVous extends Component {


    state = {
        rendezVous: [],
        open: true
    }

    fetchRendezVous = () => {
        axios.post(baseUrl + endpoints.getRendezVous + JSON.parse(localStorage.getItem("data")).username, {}).then(response => {
            console.log(response)
            this.setState({ rendezVous: response.data })
            this.setState({ open: false })
        }).catch(error => {
            console.log(error);
        })
    }
    componentDidMount() {
        this.fetchRendezVous();
    }
    render() {

        let mappedRendezVous = [];
        if (this.state.rendezVous.length > 0) {
            mappedRendezVous = this.state.rendezVous.map(element => {
                return (
                    <Animated animationIn="fadeInUp" animationOut="fadeOut" isVisible={true}>
                        <div className={`${styles.rendezVousItem} card margin-top margin-bottom`}>
                            <div>
                                <Icon name="calendar times outline" />
                            </div>
                            <div>
                                <div>
                                    <strong> Date:</strong> {element.dateDePrise}
                                </div>
                                <div>
                                    <strong>Lieu: </strong> {element.nomSpecialite}
                                </div>
                                <div>
                                    <strong>Hôpital:</strong> {element.nomEtablissement}
                                </div>
                                <div>
                                    <strong>Statut:</strong> {element.dejapaye == true ? "Payé" : "Non payé"}
                                </div>
                                <div>
                                    <strong>Résultat:</strong> {element.resultat == null ? "Pas disponible" : element.resultat}
                                </div>
                            </div>
                        </div>
                    </Animated>

                );
            })
        }

        return (
            <div className={`${styles.pageContainer}`}>
                <div>
                    <div>
                        Rendez-vous
                    </div>
                </div>
                <div style={{ width: "85%" }}>
                    {mappedRendezVous.length > 0 ? mappedRendezVous : <center>Aucun rendez vous planifié</center>}
                </div>
                <Modal closeOnDimmerClick={false} open={this.state.open}>
                    <Loader message={"Chargement des rendez-vous"} />
                </Modal>
            </div>
        )
    }
}
