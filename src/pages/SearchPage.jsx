import React, { useState } from 'react'
import _, { result } from 'lodash'
import { Link } from 'react-router-dom';
import { Search, List } from 'semantic-ui-react'
import { endpoints, baseUrl } from '../utils/config';
import axios from 'axios';

let source = [

]

const initialState = {
    loading: false,
    results: [],
    value: '',
}

function searchReducer(state, action) {
    switch (action.type) {
        case 'CLEAN_QUERY':
            return initialState
        case 'START_SEARCH':
            return { ...state, loading: true, value: action.query }
        case 'FINISH_SEARCH':
            return { ...state, loading: false, results: action.results }
        case 'UPDATE_SELECTION':
            return { ...state, value: action.selection }
        default:
            throw new Error()
    }
}



const SearchPage = ({ styles }) => {
    const [state, dispatch] = React.useReducer(searchReducer, initialState)
    const { loading, results, value } = state
    const [searchTitle, setSearchTitle] = useState("block")
    const [ResultsHeight, setResultsHeight] = useState("0%")
    const [resultsOpacity, setresultsOpacity] = useState("0")
    const [resultDivOverflow, setresultDivOverflow] = useState("hidden")
    const timeoutRef = React.useRef()
    const handleSearchChange = React.useCallback((e, data) => {
        clearTimeout(timeoutRef.current)
        dispatch({ type: 'START_SEARCH', query: data.value })
        axios.get(baseUrl + endpoints.recherche, {
            params: {
                mc: ""
            }
        }).then(result => {
            source = []
            result.data.forEach(element => {
                source.push({ title: element.nomstructure, data: element, description: element.description, image: "https://img.icons8.com/android/24/000000/hospital-2.png" })
            })
            console.log(source)
        });
        timeoutRef.current = setTimeout(() => {
            if (data.value.length === 0) {
                dispatch({ type: 'CLEAN_QUERY' })
                return
            }
            const re = new RegExp(_.escapeRegExp(data.value), 'i')
            const isMatch = (result) => {
                console.log("description")
                console.log(result)
                return re.test(result.title) || re.test(result.data.description);
            };
            console.log(re.test(result.nomstructure))
            dispatch({
                type: 'FINISH_SEARCH',
                results: _.filter(source, isMatch),
            })
        }, 300)
    }, [])

    React.useEffect(() => {
        return () => {
            clearTimeout(timeoutRef.current)
        }
    }, [])

    return (
        <div className={`${styles.pageContainer} `}>
            <div className="" style={{ display: searchTitle }}>
                Health News
            </div>
            <Search fluid placeholder="cherchez un hopital"
                loading={loading}
                onResultSelect={(e, data) => {
                    setResultsHeight("88%");
                    setresultsOpacity("1");
                    setSearchTitle("none");
                    setresultDivOverflow("initial");
                    dispatch({ type: 'UPDATE_SELECTION', selection: data.result.title });
                    source[0] = data.result;
                }
                }
                onSearchChange={handleSearchChange}
                results={results}
                value={value}
            />
            <div style={{ height: ResultsHeight, overflow: resultDivOverflow, opacity: resultsOpacity }} className={`${styles.resultsContainer}`}>
                <List divided relaxed>
                    {
                        source.map(element =>
                            (<List.Item>
                                <List.Icon name='hospital outline' size='large' verticalAlign='middle' />
                                <List.Content>
                                    <List.Header as='a'>
                                        <Link data={element.data} to={`/home/hospital/specialite/${element.data.idstructure}/${element.data.nomstructure}`}>
                                            {element.data.nomstructure}
                                        </Link>
                                    </List.Header>
                                    <List.Description data={element.data} >
                                        {element.data.description}
                                    </List.Description>
                                </List.Content>
                            </List.Item>)
                        )
                    }

                </List>
            </div>
        </div >
    );
}


export default SearchPage;