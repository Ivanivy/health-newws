import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from '../styles/user.module.css';
import { Menu, List, Modal, MenuItem } from 'semantic-ui-react'
import { Route, withRouter } from 'react-router-dom';
import { endpoints, baseUrl } from '../utils/config';
import Loader from '../components/global/Loader';
import axios from 'axios';
class User extends Component {

    state = {
        selected: 2,
        transactions: null,
        open: true,
        selectedTab: 2,
    }

    fetchTransactions = () => {
        axios.get(baseUrl + endpoints.transactions + JSON.parse(localStorage.getItem("data")).id).then(response => {
            console.log(response);
            this.setState({ open: true })
            this.setState({ transactions: response.data })
            this.setState({ open: false })
        }).catch(error => {
            console.log(error);
        })
    }

    menuItemHandlClick = (tabId, path) => {
        this.props.history.push(path);
        this.setState({ selectedTab: tabId });
    }

    componentDidMount() {
        this.fetchTransactions();
    }
    render() {



        let mappedTransactions = [];
        if (this.state.transactions) {
            mappedTransactions = this.state.transactions.map(element => {
                return (
                    <List.Item>
                        <List.Icon name='exchange' size='large' verticalAlign='middle' />
                        <List.Content>
                            <List.Description as='a'>
                                <div>
                                    <div><h4>{element.datetransaction}</h4> </div>
                                    <div><strong>{element.couttransaction} FCFA</strong></div>
                                    <div> <strong>motif:</strong>{element.motif}</div>
                                </div>
                            </List.Description>
                        </List.Content>
                    </List.Item>
                )
            })
        }
        return (
            <div className={`${styles.pageContainer}`}>

                <Menu>
                    <MenuItem onClick={() => { this.menuItemHandlClick(1, "/home/user/infos") }} active={this.state.selectedTab == 1 ? true : false}>
                        Mes infos
                    </MenuItem>
                    <MenuItem onClick={() => { this.menuItemHandlClick(2, "/home/user/transactions") }} active={this.state.selectedTab == 2 ? true : false}>
                        Transactions
                    </MenuItem>
                </Menu>
                <Route path="/home/user/infos" exact>
                    <div className={`${styles.infosContainer}`}>
                        <div>
                            <strong>Email:</strong>
                            {JSON.parse(localStorage.getItem("data")).email}
                        </div>
                        <div>
                            <strong>Id Utilisateur:</strong>
                            {JSON.parse(localStorage.getItem("data")).id}
                        </div>
                        <div>
                            <strong>Consultation effectuées:</strong>
                            35
                    </div>
                        <div>
                            <strong>Publications:</strong>
                            475
                    </div>
                        <div>
                            <strong>Rendez vous:</strong>
                            15
                    </div>
                        <div>
                            <strong>Date inscription:</strong>
                            13-06-2020
                    </div>
                        <div>
                            <strong>Hôpitaux visités:</strong>
                        20
                    </div>
                    </div>
                </Route>
                <Route path="/home/user/transactions" exact>
                    <div style={{ width: "85%" }}>

                        {mappedTransactions.length > 0 ? <List divided relaxed> {mappedTransactions}</List> : <center>Aucune Transaction effectuée</center>}

                    </div>
                </Route>
                <Modal closeOnDimmerClick={false} open={this.state.open}>
                    <Loader message={"Chargement de transactions"} />
                </Modal>
            </div>
        )
    }
}

export default withRouter(User);