const baseUrl = "http://206.189.121.189:8080/healthnews/api";
const endpoints = {
    register: "/auth/signup",
    login: "/auth/signin",
    recherche: "/services/recherche",
    speicialites: "/services/rechercheStructure?idS=",
    publications: "/services/listPublStruc?idStruc=",
    publish: "/services/ajoutPubliStruc?",
    comment: "/services/commenter?idpub=",
    transactions: "/services/getTransaction?ids=",
    getRendezVous: "/services/rechercherRendezVous?nomU=",
    createRendezVosu: "/services/ajoutRendezVous"
}
export {
    baseUrl,
    endpoints,
}